[![PyPI version](https://badge.fury.io/py/nerdvision.svg)](https://pypi.org/project/nerdvision/)

## Install Guide

1. Install the nerdvision module from <a href="https://pypi.org/project/nerdvision/" target="_blank">PyPI</a>
    ```bash
    pip install nerdvision
    ```
1. Now in your code you need to add the `@nv_serverless` annotation to the function that is the entry of the lambda function. 
    ```python
    from nerdvision import nv_serverless

    @nv_serverless
    def handler(event, context):
        event_name = event['name']
        resp = {
            'name': event_name
        }
        return resp
    ```
1. Add an environment variable `NV_API_KEY` to the lambda environment, with your API key `[API_KEY]`
1. Deploy your lambda with the changes

## What's next?

After you install the agent, here are some suggestions for what to do next:

*  Open the <a href="https://app.nerd.vision/debugger/" target="_blank">debugger</a>
*  Set a tracepoint on a line of real code (not comments) by clicking in the gutter.
*  Execute an action in the app that will cause the tracepoint to fire and a snapshot should arrive.
*  Look at the snapshot to see what live data from you app looks like.
*  Debug all your apps!

## Need more help?
*  Read the full docs <a href="https://docs.nerd.vision/python/lambda/" target="_blank">here</a>
*  Contact Support at [support@nerd.vision](mailto:support@nerd.vision)
*  You can also find us on <a href="https://twitter.com/nerdvision" target="_blank">Twitter</a>and <a href="https://www.facebook.com/NerdVision-366820847257558/" target="_blank">Facebook</a>
*  Or come and chat on <a href="https://discord.gg/TxPG97U" target="_blank">Discord</a>
