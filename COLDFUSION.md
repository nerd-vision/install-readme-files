[![Maven Central](https://img.shields.io/maven-central/v/com.nerdvision/agent?label=Agent%20Version)](https://search.maven.org/artifact/com.nerdvision/agent)

## Adobe Coldfusion 10 / 11 / 2016 / 2018 / 2021

1. Download the latest agent from <a href="https://repository.sonatype.org/service/local/artifact/maven/redirect?r=central-proxy&g=com.nerdvision&a=agent&v=LATEST" target="_blank">here</a>
    
    ---
    > **NOTE:** Please ensure you rename the file to match that which is added to the jvm.config file.

    ---
1. Navigate to the following directory: **{ColdFusion-Home}/cfusion/bin**`
1. Locate and open the **jvm.config** file with a text editor of your preference.
1. Add the NerdVision agent to the end of the line after "# Arguments to VM". 
    ```bash
    # Arguments to VMx
    java.args=-server ... -javaagent:/full/path/to/nerdvision.jar=api.key=[API_KEY] 
    ```
    ---
    > **Warning**: Please make sure that all the Java Arguments are on the same line.

    ---
1. Save the changes made.
1. Restart the server to start with NerdVision

## What's next?

After you install the agent, here are some suggestions for what to do next:

*  Open the <a href="https://app.nerd.vision/debugger/" target="_blank">debugger</a>
*  Set a tracepoint on a line of real code (not comments) by clicking in the gutter.
*  Execute an action in the app that will cause the tracepoint to fire and a snapshot should arrive.
*  Look at the snapshot to see what live data from you app looks like.
*  Debug all your apps!

## Need more help?
*  Read the full docs <a href="https://docs.nerd.vision/coldfusion/install/" target="_blank">here</a>
*  Contact Support at [support@nerd.vision](mailto:support@nerd.vision)
*  You can also find us on <a href="https://twitter.com/nerdvision" target="_blank">Twitter</a>and <a href="https://www.facebook.com/NerdVision-366820847257558/" target="_blank">Facebook</a>
*  Or come and chat on <a href="https://discord.gg/TxPG97U" target="_blank">Discord</a>
