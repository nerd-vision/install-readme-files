[![Nuget](https://img.shields.io/nuget/v/NerdVision?style=flat-square)](https://www.nuget.org/packages/NerdVision/)

## Install Guide

1. Install the NerdVision agent dependency from <a href="https://www.nuget.org/packages/NerdVision/" target="_blank">nuget package manager</a>.
    ```bash
    dotnet add package NerdVision
    ```
1. Once the agent has been installed, we need to activate the agent. This call should be as early in the application as possible.
    ```cs
    using NerdVision;
    
    NV.Start("[API_KEY]");
    ```
1. We also require that the following settings have been configured in the project settings:
    - Debugging information - should be set to 'Portable' (required)
    - Optimize code - should be disabled (optional) - see <a href="https://docs.nerd.vision/dotnet/install/#optimize-code" target="_blank">Optimize code</a>

## What's next?

After you install the agent, here are some suggestions for what to do next:

*  Open the <a href="https://app.nerd.vision/debugger/" target="_blank">debugger</a>
*  Set a tracepoint on a line of real code (not comments) by clicking in the gutter.
*  Execute an action in the app that will cause the tracepoint to fire and a snapshot should arrive.
*  Look at the snapshot to see what live data from you app looks like.
*  Debug all your apps!

## Need more help?
*  Read the full docs <a href="https://docs.nerd.vision/dotnet/install/" target="_blank">here</a>
*  Contact Support at [support@nerd.vision](mailto:support@nerd.vision)
*  You can also find us on <a href="https://twitter.com/nerdvision" target="_blank">Twitter</a>and <a href="https://www.facebook.com/NerdVision-366820847257558/" target="_blank">Facebook</a>
*  Or come and chat on <a href="https://discord.gg/TxPG97U" target="_blank">Discord</a>
