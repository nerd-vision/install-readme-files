[![Maven Central](https://img.shields.io/maven-central/v/com.nerdvision/agent)](https://search.maven.org/artifact/com.nerdvision/agent)
## Install Guide

1. Install the NerdVision dependency into your project. We recommend using maven or gradle.
    **Using Maven**
    ```xml
    <dependency>
        <groupId>com.nerdvision</groupId>
        <artifactId>nerdvision</artifactId>
        <version>LATEST</version>
    </dependency>
    ```

    **Using Gradle**

    ```typescript
    implementation 'com.nerdvision:nerdvision:LATEST'
    ```
    
1. Now you need to configure NerdVision in your code add a call to NerdVision. This should go as the first line in the lambda function.
    ```java
    import com.nerdvision.api.NerdVision;

    public class MyLambda implements Function {
        public Object apply(Object incoming) {
            NerdVision.lambda("[API_KEY]");
        }
    }
    ```
1. Deploy your lambda with the changes

---
> **Warning: Lambdas need to be invoked twice**
>
>  Currently the java agent is only able to track calls on lambdas on the second invocation after the tracepoint has been installed. The invocation of the lambda does not have to trigger the tracepoint twice, the lambda just needs to run twice.

---

## What's next?

After you install the agent, here are some suggestions for what to do next:

*  Open the <a href="https://app.nerd.vision/debugger/" target="_blank">debugger</a>
*  Set a tracepoint on a line of real code (not comments) by clicking in the gutter.
*  Execute an action in the app that will cause the tracepoint to fire and a snapshot should arrive.
*  Look at the snapshot to see what live data from you app looks like.
*  Debug all your apps!

## Need more help?
*  Read the full docs <a href="https://docs.nerd.vision/java/lambda/" target="_blank">here</a>
*  Contact Support at [support@nerd.vision](mailto:support@nerd.vision)
*  You can also find us on <a href="https://twitter.com/nerdvision" target="_blank">Twitter</a>and <a href="https://www.facebook.com/NerdVision-366820847257558/" target="_blank">Facebook</a>
*  Or come and chat on <a href="https://discord.gg/TxPG97U" target="_blank">Discord</a>
