[![Available as beta](https://img.shields.io/badge/Available%20as-beta-blue?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAKbnpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHja5VlrduwsDvyvVcwSDJjXcniYc2YHs/wpCbBxp5107nd/zaRPYgfbslAVoqSm4z//bvQv/NjNBdqtDy46t+Fnj3vUCSdh6z9R/qptl7/ys49L+P82TucFjSGDo+n/+jTuTxi31wPzHSrfxymMKzoMQ+o0LD+G38zndXUS47qPq+EJxaOfuBj86moehsp0OVy/++nWmC7+p9uAR5SqxYuM1odRZpO/oXtg+m/Cb8BfbSzuU/gks5udZGh6goDcpjeP27YG6BbkeUav0T/PXoKv0xg3L7F0I0Y4eXtB2Zdxc75Gry82p0f6fiFapb9MZ/y2VkNrR59d2h0i6gajJNhqmsGNGSE38pjDx+PX4tzLJ+ITtrQVQF63smV8iopKA5VGaldVJdXUIceiClzc9aE9jloXbWQsGK+jLoZx2vmjmvYmmgoEtSn6IGMwrE9flLw3yvuKCnhzVbhVKxhTeOTxQ99d/M2HWiscIrWFM1bwS3PA4QYjx39xFwBRbeBmJcDzM+DfFv6AqkDQSpgDJpi23E1kqy5uGcHZ4D6LY19CinwdBhAivBvgIxZAYHPKWOXU5rX2SiGOAQAleI6FoDMQUNbqCif1bozT5HXQ/G4845Xcq612moeRmwCENc54YBNNAlj7bsEfvwdwKFljd2uts94GstEmZ9zurHPOO05yyRu/e+ud9z746FMwYQ82uOBDCDGkqKNBDrTRRR9DjDElTQkvSrCVcH/CSNbZ5D3b7LLPIcecCuhT9mKLK76EEkuqupqKNFFd9TXUWNOh6ECmOPbDHu7wRzjikRq41kzbm22u+RZabOlEbaD65fML1NRATQtSfJ8/UcMoeT9NKE4nljEDYnpXQNwzAiC0Zsy2oPZdM3KM2RY1FoXVcNIyNlQVIwYI90Np29SJ3YXcR7iRDR/hpn9Cjhi6v4EcAbqvuL1BrfI+VwSxvgo5ppvB6sP1IyTSIfGmlv7p8f/CUK6bOVT0hRMvsr8NFRGOfK6Mb0fhs7z5aFvTRwU0rbbMORpYGkc+NwtTgAm2DlwIzrat5HYoi2dBCayRKkZ2c9R2WAzkDHvZOMfjzdVsAH87atW59KGY+BhxF4aPPhagQ3xAytstS4+HI/10w6dHGIKkksDwnNqWVW7ZmnZo+F0RIrc1Df9szaVyULxM0+UAljd/ZIt5Y4SK6cEF3SUSkQNyIIKN8wliizxyvmXb0uGv9+ApbK04wZtIXpVqzECmZVdjQWjcEe0Rpu++tB7sijjj3iZLR0ZcPrCJt7gZYtMCS8c74g6lg6DNWGd+AEtvoG0gIwTtFWwmE2I0TjZmkrwZZIK5IzlBrSQno64mTGi5JBcEfDYohn4y0BJLVQM5kxC4hWjIdyfRiN09ijp5loHLMUwgWdaIKTlmahJjEhvY4tgnvrnpPn2q2QVZEq7j0q8iLlHoP6JpEEMMyFrpjLe8K2C2g++OZBogdqc836JA+M73sG0vKwZ+zjXzsmJoLBmZymkhH/uIx+mGxEN87o70gOBJ8SJnQkT6m9QVkYLcuS1wPh3XhEHtyhczW4xcgTgt2QIszheDkMWPEfsRX3oM8PsMlp9wI37TfPI7eL+3f8uQ3xrBS8K72J+Rpx76QQFho1DgFvksXE8nva/IXIGhL5G5hfBdBEN7YvZpAb7c0r83t/Rft5Z9m8nfulKH6XJ4j7Xmd1kCNTnwF0pCMo3nWyBECrIneBzktZI95SWHtmrmT36HPcj3BOqQ+LYrfWJ2Ix37yhiUzK62ojM2+rq3YRnVta8ZGUViFL1DrsQ6k1SLLOu8yfA4Wdk9Pszn9D6h90Ty1RFEjl25HDm9oJ7qTyewpfzZpkQfP2A7YYHf2EG6nyV3P2n309HKjsYRLr7FXH6GFbzthO8EDzw68XsL374pgQB7G29GlRcEmNQpUK09tpepqb7P507FUqoRcA+8LNs0Nq+e/hg/FA1M0HFXBkc2+gvbvhzfG5KNmmMyNuorJn2jlqjMoHRRQD0qK4V4qwYVu9ARNnVkrPH83NjaFfOdL4xhMoOMlSvAaYFN3G4771JPpqlbsE3/CPD3+BIjhhVqxRsYBMa+jJOtg2vfH4Whk5+FTnqGeOMmL+b37JRuF+w0szKMXigGYyv3uFfCE8QFlH37fJ659nIXjdv8A3dvzz8yHI/T9+//mYg+9m2cJlAiDpc02sUh6p8BlKhDAaqrQ1lAjFTo3tLzgiy/IgR9m7Dv62Zmb4FbFzezN6ONXaQGU9KEG1Rl0cA+Fe74ZVNbArHg/7f5AWLUjA19CpYWRDD2LHQo2aOS5XZNYsXrUjqwhU4pPIuf6EWNTD3cqx/ePR/0cFfDbyUTvbvwnT9T1y5aQsQgXTJO1OBUEu1VxbEYFGHQ2qlcquhp5zhO5PdeU9RRfMxlOYn2Kc/oLdE4q1ifO6OMZEmjISBqHruxSixTVqVFr9Ju6suJyaJIGJJFkQCQU5EERyxJpiKZQViiz2xgnTVo0Rnyg9BigbSN+54l4JMEo5uJIdFvSt/OoJ/VQluq4zKlPsWzOn4V6c+V0LtCiL5UQiIBj4dIPCtJWgXww/y6DF5r/6GDl/k5WioZmZ1Z8b/Qf8G+l6cTfTgSyF34J1bUDHle4zWqgUWr5k5V1qpf1MhTWv5IpcgU6Pccvk1BSMum6M/7M1e9xWuLpJz5Va34plQM3EDIkl+2K9uIp5xL2dE+4+hHWTjmm2dZ2GeMO/KKmu8E/Jr9Pkl+xNmvng2Cn5sJ+/a+VUU/96o+K93pjYWX0r3KwolvN6FrD6IPNiH4IiBLbuP/4BP30k5E2DAtlm/cySt3zJ0727ojjTjRrcMxQVmipJb2BJjCNmbyNbNa4rjRnYBX0pp5cCHg0r2pPYEvzRd66r5813xhO7JHLimBrvqV1/q3MvZ2tM2ONt6ospf6chVQUmFORc2CfNFPsnvOnuOQYYnOduC1D08jZy9wO3uBrC1tqyNZrSqfvi/Cn2vwVzUnhLRNzlu7as5T0X8q6Mnc6uLfq5ApQugfqhDpXHPyoT/LPlN6tVNw0VRcp+C6RP9ZuYlzVouAOOwuPmTbq5sJG1094VOM9zA8Sf4zAukm6On3Jd57Qf9Bh93XhSJ+9kxmN2IWAMSJQGqAWQKsBcAq/zszOpgXlBNIrrKPH9t05ev3HV+6l/RJ+3L74BsT+mhLnqLkffsMJVvb6Kls+rxqYjMl07I0LzNnw2pdmt3MrdrO57rkJXJPJFf3bGl8fOl7CCM7LUauoNFE44n1ZNG7eX1qI124MTX2Oq18KNxH6RSn2ZJZ0wV7dUsX86sT8Wstjq+WIM2yd/n25Za3l296lrn1Rsi1AYx+9vMXOU/f4yyujIdptifnVF6flmbPkvq4vSTuvHQAaW0BftYBnORZzRRHdzMnWBcL423pYvh9N4g+21dnffo30ghvMtyi6gaxdS3/eTH01DT9Xc+U/rSn1rfba9Z0VwPpjRQYlFjEAGu6224IOUDrtLdtt/f/P+8G0qeu/3Skj5XV/74hrr1rxIP/BXCa6jugOoIdAAABhWlDQ1BJQ0MgcHJvZmlsZQAAeJx9kT1Iw0AcxV9TpVUqDnZQcYhQO1kQFXHUKhShQqgVWnUwufRDaNKQtLg4Cq4FBz8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLGg+N+vLv3uHsHCPUS06yOMUDTK2YqERcz2RUx8IogBtCFKIZlZhmzkpSE5/i6h4+vdzGe5X3uz9Gj5iwG+ETiGWaYFeJ14qnNisF5nzjMirJKfE48atIFiR+5rrj8xrngsMAzw2Y6NUccJhYLbay0MSuaGvEkcUTVdMoXMi6rnLc4a6Uqa96TvzCU05eXuE5zCAksYBESRCioYgMlVBCjVSfFQor24x7+QccvkUsh1wYYOeZRhgbZ8YP/we9urfzEuJsUigOdL7b9MQIEdoFGzba/j227cQL4n4ErveUv14HpT9JrLS1yBPRuAxfXLU3ZAy53gP4nQzZlR/LTFPJ54P2MvikL9N0C3atub819nD4AaeoqeQMcHALRAmWvebw72N7bv2ea/f0AhJJyrjxNWaIAAAMAUExURQAAAP///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wc9CwgAAAABdFJOUwBA5thmAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB+QIGQksGQ7rWAEAAADcSURBVFjD7ZbBDsMwCEPx///0Llk0KAETuk6awqVqZ7+sbkIicurUfxeArh9N+xqQorH0IyYrkZEB/vO1HY6VCEYrcakkZyRldUU7jKRqRxhG0y4P228efuMPWNCPAN1XaGZApBih488QL1FfQax1agSuU8guYMh2mjAxTgUgXwFw+xAJcJpVHuIELBdaBpC5D1wGVb8mgM/3MLvou6fTCQ1gYbL5TaLypT0AOZGHQAxALEDyndncaABzrjF6nYGUCGgC3HlQOBvOvO2VXHD5WaTSt9A9sp469Ui9AIkuBC4N8u42AAAAAElFTkSuQmCC)](https://nerd.vision/pricing)
# <i class="fad fa-chart-network fa-lg"></i> LocalProxy

This is a service which acts as a proxy between NerdVision clients and NerdVision servers.
It also allows for the proxied data to be stored locally or in the users cloud.

The LocalProxy is currently in Beta. To get an API-Key for the LocalProxy service, contact us at [<i class="fas fa-at"></i> support@nerd.vision](mailto:support@nerd.vision).

!!! Warning "Security"
    The LocalProxy service is intended to be used behind a secure network. This version has no UI or
    client validation meaning that if used on the open internet, there is potential for anybody
    to send/receive data from the proxy.
    
    For the LocalProxy to work correctly, both the clients and the NerdVision UI need to be accessed from the same network as the 
    proxy.
    
    We are actively working on adding client and UI auth. The current version is safe to be used
    behind a secure network.
    
    To use the LocalProxy you will **need** to set up TLS by providing cert files to the LocalProxy service or
    running the LocalProxy behind a reverse proxy.

## Getting started

For storage specific information, refer to the following pages:


<h3><img src="https://branding.nerd.vision/external_assets/cloud-providers/aws_logo.svg" alt="drawing" width="25"/><span> AWS S3</span></h3> 

To get started you need to run the LocalProxy service and configure the NerdVision client to send requests through the LocalProxy service.

The LocalProxy is available on [DockerHub](https://hub.docker.com/r/nerdvision/localproxy).
You can run the LocalProxy service by executing:
```
docker run -p 8080:8080 -e LOCAL_PROXY_STORAGE_PROVIDER=<provider> -e AWS_REGION=<aws_region> -e AWS_ACCESS_KEY_ID=<access_key_id> -e AWS_SECRET_ACCESS_KEY=<secret_access_key> -e LOCAL_PROXY_TOKEN=<token> -e LOCAL_PROXY_CALLBACK_URL=<callkback_url> nerdvision/localproxy
```

<h3><img src="https://branding.nerd.vision/external_assets/languages/node.svg" alt="drawing" width="25"/><span> Node.js</span></h3>

The Node.js client can be configured to send snapshots to the LocalProxy as shown here:

```
nerdvision.start({
    api_key: '<YOUR_API_KEY>',
    event_snapshot_url: '<YOUR_LOCAL_PROXY_URL>', # E.g. https://api.example.com:8080
    event_snapshot_api: '/localproxy/v1/context/eventsnapshot'
}, false);
```

<h3><img src="https://branding.nerd.vision/external_assets/languages/python.svg" alt="drawing" width="25"/><span> Python</span></h3>

The Python client can be configured to send snapshots to the LocalProxy as shown here:

```
nerdvision.start(api_key="<YOUR_API_KEY>",
                 agent_settings={
                     'event_snapshot_url': "<YOUR_LOCAL_PROXY_URL>", # E.g. https://api.example.com:8080
                     'event_snapshot_api': "/localproxy/v1/context/"
                 })
```

<h3><img src="https://branding.nerd.vision/external_assets/languages/java.svg" alt="drawing" width="25"/><span> Java</span></h3>

The Java client can be configured to send snapshots to the LocalProxy as shown here:
```
-javaagent:/full/path/to/nerdvision.jar=api.key=<YOUR_API_KEY>,event.snapshot.url=<YOUR_LOCAL_PROXY_URL>,event.snapshot.api=/localproxy/v1/context
```
E.g.
```
-javaagent:nerdvision.jar=api.key=nv-abc,event.snapshot.url=https://api.example.com:8080,event.snapshot.api=/localproxy/v1/context
```

<h3><img src="https://branding.nerd.vision/external_assets/languages/dotnet.svg" alt="drawing" width="25"/><span> .NET</span></h3>

The .Net client can be configured to send snapshots to the LocalProxy by setting the following environment variables:

| Name | Value | Description |
| --- | --- | --- |
| NV_EVENT_SNAPSHOT_URL | `<YOUR_LOCAL_PROXY_URL>` (e.g https://api.example.com:8080) |  The URL to the LocalProxy. |
| NV_EVENT_SNAPSHOT_API | `/localproxy/v1/context/eventsnapshot` | The path to use when uploading snapshots. |
