[![Version](https://img.shields.io/badge/Module%20Version-1.0.6-blue?style=flat-square?logo=https://branding.nerd.vision/external_assets/languages/commandbox.svg)](https://www.forgebox.io/view/nerdvision) [![Maven Central](https://img.shields.io/maven-central/v/com.nerdvision/agent?label=Agent%20Version)](https://search.maven.org/artifact/com.nerdvision/agent)

## CommandBox
---
> **NOTE:** This module only needs to be installed once per server. View the full module docs on <a href="https://www.forgebox.io/view/nerdvision" target="_blank">ForgeBox</a>.

---

1. Install the NerdVision module with the command:
    ```bash
    box install nerdvision
    ```
1. Configure the agent by setting the API key.
    ```bash
    box config set modules.nerdvision.apikey=[API_KEY]
    ```
1. Restart the server to start with NerdVision

## What's next?

After you install the agent, here are some suggestions for what to do next:

*  Open the <a href="https://app.nerd.vision/debugger/" target="_blank">debugger</a>
*  Set a tracepoint on a line of real code (not comments) by clicking in the gutter.
*  Execute an action in the app that will cause the tracepoint to fire and a snapshot should arrive.
*  Look at the snapshot to see what live data from you app looks like.
*  Debug all your apps!

## Need more help?
*  Read the full docs <a href="https://docs.nerd.vision/commandbox/install/" target="_blank">here</a>
*  Contact Support at [support@nerd.vision](mailto:support@nerd.vision)
*  You can also find us on <a href="https://twitter.com/nerdvision" target="_blank">Twitter</a>and <a href="https://www.facebook.com/NerdVision-366820847257558/" target="_blank">Facebook</a>
*  Or come and chat on <a href="https://discord.gg/TxPG97U" target="_blank">Discord</a>
